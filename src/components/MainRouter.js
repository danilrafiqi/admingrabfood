import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import {Switch, Route} from 'react-router-dom';
// import Dashboard from './components/Dashboard';
import Home from '../containers/home/';
import {RestaurantList, FoodList} from '../containers'
// import Coba from '../containers/restaurant/cobatabel'
import SimpleTable from './SimpleTable';


const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
});

class MainRouter extends Component {
	render() {
		const { classes } = this.props;
		return (
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
	          <Typography variant="display1" noWrap>{'You think water moves fast? You should see ice.'}
		        <Switch>
		            <Route exact path="/" component={RestaurantList}/>
		        </Switch>

		        <Switch>
		            <Route path="/foods" component={FoodList}/>
		            <Route path="/restaurants" component={RestaurantList }/>
		        </Switch>
	          </Typography>
          </main>

		);
	}
}


export default  withStyles(styles)(MainRouter);