import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';

//import inputan
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';

import AddIcon from '@material-ui/icons/Add';
import SaveIcon from '@material-ui/icons/Save';
import Axios from 'axios';


function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
//style inputan
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
  },


  button: {
    margin: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },

});

class Add extends React.Component {
  state = {
    open: false,
    food_name:'',
    price:'',
    images:'',
    restaurant_id:''
  };


  handleChange = e => {
    this.setState({
    	[e.target.name]: e.target.value
    });
  };


  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  createFood = () => {
    Axios
      .post('https://servergrabfood.herokuapp.com/apiv1/foods/', {
        food_name:this.state.food_name,
        price:this.state.price,
        images:this.state.images,
        restaurant_id:this.state.restaurant_id
      })
      .then(res => {
        this.setState({
          open: false,
          food_name:'',
          price:'',
          images:'',
          restaurant_id:''
        });
        this.props.getData();
      });
  };

  render() {
    const { classes } = this.props;

    return (
      <div>
      <Button onClick={this.handleOpen} className={classes.button} variant="contained" color="secondary" >
        <AddIcon  />
      </Button>

        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
            <Typography variant="title" id="modal-title">
              Add Data Food
            </Typography>
            <br/>
            <Typography variant="subheading" id="simple-modal-description">





		      <div className={classes.container}>
		        <FormControl className={classes.formControl}>
		          <InputLabel htmlFor="">Food Name</InputLabel>
		          <Input id="" name='food_name' value={this.state.food_name} fullWidth={true} onChange={this.handleChange} />
		        </FormControl>

		        <FormControl className={classes.formControl}>
		          <InputLabel htmlFor="">Price</InputLabel>
		          <Input id="" name='price' value={this.state.price} fullWidth={true} onChange={this.handleChange} />
		        </FormControl>

		        <FormControl className={classes.formControl}>
		          <InputLabel htmlFor="">Image Url</InputLabel>
		          <Input id="" name='images' value={this.state.images} fullWidth={true} onChange={this.handleChange} />
		        </FormControl>

            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="">Restaurant Id</InputLabel>
              <Input id="" name='restaurant_id' value={this.state.restaurant_id} fullWidth={true} onChange={this.handleChange} />
            </FormControl>
		      </div>



            </Typography>

      <Button onClick={this.createFood} className={classes.button} variant="contained" color="secondary" >
        Save
        <SaveIcon className={classes.rightIcon}  />
      </Button>

          </div>
        </Modal>
      </div>
    );
  }
}

Add.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Add);