import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';


import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';


import Add from './Add'
import Update from './Update'

const styles = {
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
};

class List extends Component {
	state = {
		data:[],
		loading:false
	}
	all = () => {
		Axios
			.get('https://servergrabfood.herokuapp.com/apiv1/foods')
			.then(res =>{
				this.setState({
					data:res.data,
					loading:true
				})
			})
	}

	delete = (id) => {
		Axios
			.delete(`https://servergrabfood.herokuapp.com/apiv1/foods/${id}`)
			.then(res =>{
				this.all()
			})
	}

	componentDidMount(){
		this.all();
	}


    render() {
  	const { classes } = this.props;

	  return (
	    <Paper className={classes.root}>
	    {
	    	//buat tombol add
	    }
	    <Add getData={this.all}/>
	      <Table className={classes.table}>
	        <TableHead>
	          <TableRow>
	            <TableCell>Food Name</TableCell>
	            <TableCell numeric>Price</TableCell>
	            <TableCell numeric>Image</TableCell>
	            <TableCell numeric>Restaurant</TableCell>
	            <TableCell numeric>Action</TableCell>
	          </TableRow>
	        </TableHead>

	        <TableBody>
	          {this.state.data.map((datum) => {
	            return (
	              <TableRow key={datum._id}>
	                <TableCell component="th" scope="row">
	                  {datum.food_name}
	                </TableCell>
	                <TableCell numeric>{datum.price}</TableCell>
	                <TableCell numeric>
	                	<img src={datum.images} style={{width :'30px'}}/>
	                </TableCell>
	                <TableCell numeric>{datum.restaurant_id}</TableCell>
	                <TableCell numeric>
				      <Button
					      onClick={
					      	()=>{
					      		this.delete(datum._id)
					      	}
					      }
					      variant="contained" size="small" color="secondary" >
				        <DeleteIcon/>
				      </Button>
	                	<Update getData={this.all} detail={datum._id}/>
	                </TableCell>
	              </TableRow>
	            );
	          })}
	        </TableBody>
	      </Table>
	    </Paper>
	  );
    }
}

List.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(List);






