import {
    List as RestaurantList,
    Add as RestaurantAdd,
    Update as RestaurantUpdate
} from './restaurant';

import {
    List as FoodList,
    Add as FoodAdd,
    Update as FoodUpdate
} from './food';


export {
   RestaurantList,
   RestaurantAdd,
   RestaurantUpdate,
   FoodAdd,
   FoodUpdate,
   FoodList
}